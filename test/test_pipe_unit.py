import io
import os
import sys
from unittest import mock
from contextlib import contextmanager

from bitbucket_pipes_toolkit.test import PipeTestCase

from pipe.pipe import SlackNotifyPipe, schema


TEST_WEBHOOK_URL = "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


@mock.patch('requests.post')
@mock.patch.dict(os.environ, {'WEBHOOK_URL': TEST_WEBHOOK_URL, 'MESSAGE': 'Hello!'})
class SlackNotifyTestCase(PipeTestCase):

    def test_notify_succeeded(self, slack_status_mock):

        slack_status_mock.return_value.status_code = 200
        slack_status_mock.return_value.text.return_value = 'ok'

        with capture_output() as out:
            SlackNotifyPipe(schema=schema, check_for_newer_version=True).run()

        self.assertRegex(out.getvalue(), rf"✔ Notification successful.")

    def test_notify_failed(self, slack_status_mock):

        slack_status_mock.return_value.status_code = 400
        slack_status_mock.return_value.text.return_value = 'invalid_token'

        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                SlackNotifyPipe(schema=schema, check_for_newer_version=True).run()
            self.assertEqual(exc_context.exception.code, 1)

        self.assertRegex(out.getvalue(), rf"✖ Notification failed.")
